import React from 'react'
import {Route, Redirect} from 'react-router-dom'

const PrivateRoute = ({ component: Component, ...rest }) => {
  const IsUserSignIn = localStorage.getItem('isSignIn') || false ;
  return (
  <Route {...rest} render={(props) => (
      IsUserSignIn
      ? <Component {...props} />
      : <Redirect to='/signIn' />
  )} />
)}

export default PrivateRoute;