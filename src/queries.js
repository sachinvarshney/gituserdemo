const getDogList = gql`
  query getDogById($id: String! ){
    dogs(id: $id){
      id
      name
      breed
      displayName
    }
  }
`
